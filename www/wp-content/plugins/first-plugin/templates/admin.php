<div class="warp">
    <h1>First Plugin</h1>
    <?php settings_errors(); ?>

    <form method="post" action="options.php">
        <?php
            settings_fields('first_plugin_options_group');
            do_settings_sections('first_plugin');
            submit_button();
        ?>
    </form>
</div>