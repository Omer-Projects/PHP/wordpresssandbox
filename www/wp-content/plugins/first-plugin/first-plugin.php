<?php
/**
 * @package FirstPlugin
 */
/*
Plugin Name: First Plugin
Plugin URI: 
Description: This is my first plugin
Version: 1.0.0
Author: Omer Priel
Author URI:
License: GPLv2
Text Domain: first-plugin
*/

 /*
 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.
*/

if (!defined('ABSPATH'))
{
    die;
}

if (!file_exists(dirname(__FILE__) . '/vendor/autoload.php'))
{
	die;
}
require_once dirname(__FILE__) . '/vendor/autoload.php';

// Register to the Services
Src\Init::RegisterServices();

// Activation and Deactivation
$plugin = Src\Plugin::GetPlugin();

register_activation_hook( __FILE__, array($plugin, 'Activation'));
register_deactivation_hook( __FILE__, array($plugin, 'Deactivation'));