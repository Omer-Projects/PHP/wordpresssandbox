<?php
/**
 * @package FirstPlugin
 */

namespace Src\Base;

use Src\Service;

class Enqueue extends Service
{
	public function Register()
	{
		add_action('admin_enqueue_scripts', array($this, 'EnqueueAdmin'));
		add_action('wp_enqueue_scripts', array($this, 'EnqueueFront'));
	}

	function EnqueueAdmin()
    {
        wp_enqueue_style( 'myPluginAdminStyle', $this->PLUGIN_URL . 'assets/adminStyle.css');
        wp_enqueue_script( 'myPluginAdminScript', $this->PLUGIN_URL . 'assets/adminScript.js');
    }

    function EnqueueFront()
    {
        wp_enqueue_style( 'myPluginFrontStyle', $this->PLUGIN_URL . 'assets/frontStyle.css');
        wp_enqueue_script( 'myPluginFrontScript', $this->PLUGIN_URL . 'assets/frontScript.js');
    }
}