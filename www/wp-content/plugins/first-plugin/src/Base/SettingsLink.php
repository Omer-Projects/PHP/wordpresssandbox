<?php
/**
 * @package FirstPlugin
 */

namespace Src\Base;

use Src\Service;

class SettingsLink extends Service
{	
	public function Register()
	{
		add_filter('plugin_action_links_' . $this->PLUGIN_BASENAME, array($this, 'AddSettingsLinks'));
	}

	function AddSettingsLinks($links)
	{
		$settingsLink = '<a href="admin.php?page=first_plugin">Settings</a>';
		$googleLink = '<a href="http://www.google.com" target="_blank">Google</a>';

		array_push($links, $settingsLink, $googleLink);

		return $links;
	}
}