<?php
/**
 * @package FirstPlugin
 */

namespace Src\CustomPostTypes;

use Src\Service;

class CustomPost extends Service
{
	public function Register()
	{
		add_action('init', array($this, 'AddCustomPostType'));
	}

	static public function AddCustomPostType()
    {
        register_post_type( 'CustomPost', [
            'public' => true,
            'label' => 'Custom Post'
        ]);
	}
}