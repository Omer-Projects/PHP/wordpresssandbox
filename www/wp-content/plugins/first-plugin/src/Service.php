<?php
/**
 * @package FirstPlugin
 */

namespace Src;

abstract class Service
{
	// Consts
	public ?string $PLUGIN_PATH;
	public ?string $PLUGIN_URL;
	public ?string $PLUGIN_BASENAME;

	public final function __construct()
	{
		$a = dirname(__FILE__, 2);
		$this->PLUGIN_PATH = dirname(__FILE__, 2) . '\\';
		$this->PLUGIN_URL = plugin_dir_url(dirname(__FILE__, 1));
		$this->PLUGIN_BASENAME = plugin_basename(dirname(__FILE__, 2));
	}

    public function Init() { }

	abstract public function Register();

    protected function RequireTemplate(string $name)
    {
        require_once "$this->PLUGIN_PATH/templates/$name.php";
    }
}