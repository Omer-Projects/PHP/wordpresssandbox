<?php
/**
 * @package FirstPlugin
 */

namespace Src\Pages;

use Src\Service;
use Src\API\SettingsAPI;

class Admin extends Service
{
    public SettingsAPI $Settings;

    public function Init()
    {
        $this->Settings = new SettingsAPI();
    }

	public function Register()
	{
        $this->RegisterCustomFields();

        $this->RegisterPages();
	}

    function RegisterPages()
    {
        $pages = [
            [
                'page_title' => 'First Plugin',
                'menu_title' => 'First',
                'capability' => 'manage_options',
                'menu_slug' => 'first_plugin',
                'callable' => function() { $this->RequireTemplate('admin'); },
                'icon_url' => 'dashicons-admin-settings',
                'position' => 110
            ]
        ];

        $subPages = [
            [
                'parent_slug' => 'first_plugin',
                'page_title' =>  'Custom Post Types',
                'menu_title' =>  'CPT',
                'capability' =>  'manage_options',
                'menu_slug' => 'first_plugin_cpt',
                'callable' => function() { $this->RequireTemplate('cpt'); },
            ],
            [
                'parent_slug' => 'first_plugin',
                'page_title' =>  'Custom Taxonomies',
                'menu_title' =>  'Taxonomies',
                'capability' =>  'manage_options',
                'menu_slug' => 'first_plugin_taxonomies',
                'callable' => function() { $this->RequireTemplate('taxonomies'); },
            ],
            [
                'parent_slug' => 'first_plugin',
                'page_title' =>  'Custom Widgets',
                'menu_title' =>  'Widgets',
                'capability' =>  'manage_options',
                'menu_slug' => 'first_plugin_widgets',
                'callable' => function() { $this->RequireTemplate('widgets'); },
            ]
        ];
        
        $this->Settings->SetAdminPages($pages)
            ->AddWithSubPage('Dashboard')->AddSubAdminPages($subPages)
            ->Register();
    }

    function RegisterCustomFields()
    {
        $settings = [
            [
                'option_group' => 'first_plugin_options_group',
                'option_name' => 'text_example',
                'callback' => array($this, 'Callback_OptionsGroup')
            ],
            [
                'option_group' => 'first_plugin_options_group',
                'option_name' => 'website_domain',
            ]
        ];

        $sections = [
            [
                'id' => 'first_plugin_admin_index',
                'title' => 'Settings',
                'page' => 'first_plugin',
            ]
        ];

        $fields = [
            [
                'id' => 'text_example',
                'title' => 'Text Example',
                'callback' => array($this, 'Callback_TextExample'),
                'page' => 'first_plugin',
                'section' => 'first_plugin_admin_index',
                'args' => [
                    'label_for' => 'text_example',
                    'class' => 'example-class'
                ]
            ],
            [
                'id' => 'website_domain',
                'title' => 'Website Domain',
                'callback' => array($this, 'Callback_WebsiteDomain'),
                'page' => 'first_plugin',
                'section' => 'first_plugin_admin_index',
                'args' => [
                    'label_for' => 'text_example',
                    'class' => 'example-class'
                ]
            ]
        ];

        $this->Settings->SetSettings($settings);
        $this->Settings->SetSections($sections);
        $this->Settings->SetFields($fields);
    }

    // Callbacks
    function Callback_OptionsGroup($input)
    {
        return $input;
    }
    
    function Callback_TextExample()
    {
        $value = esc_attr(get_option('text_example'));
        echo '<input type="text" class="regular-text" name="text_example" value="' . $value . '" placeholder="Write text..." />';

        // print a table
        $num = intval('0' . $value);
        
        echo '<table>';
        for ($y = 1; $y <= $num; $y++)
        {            
            echo '<tr>';
            for ($x = 1; $x <= $num; $x++)
            {
                echo '<td>' . ($x * $y) . '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
    }

    function Callback_WebsiteDomain()
    {
        $value = esc_attr(get_option('website_domain'));
        echo '<input type="text" class="regular-text" name="website_domain" value="' . $value . '" placeholder="website domain..." />';

        // print it as link
        if (!empty($value))
        {
            echo ' <a href="http://' . $value . '" target="_blank">Link</a>';
        }
    }
}