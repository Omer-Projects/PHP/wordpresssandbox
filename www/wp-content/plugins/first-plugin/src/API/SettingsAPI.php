<?php
/**
 * @package FirstPlugin
 */

namespace Src\API;

class SettingsAPI
{
    public $AdminPages = array();
    public $AdminSubPages = array();

    public $Settings = array();
    public $Sections = array();
    public $Fields = array();

    public function Register()
    {
        if (!empty($this->AdminPages))
        {
            add_action( 'admin_menu', array($this, 'AddAdminMenu'));
        }

        if (!empty($this->Settings))
        {
            add_action( 'admin_init', array($this, 'RegisterCustomFields'));
        }
    }

    public function SetAdminPages(array $pages)
    {
        $this->AdminPages = $pages;

        return $this;
    }

    public function AddWithSubPage(string $title = null)
    {
        if (empty($this->AdminPages))
        {
            return $this;
        }
        
        $adminPage = $this->AdminPages[0];

        $subPages = [
            [
                'parent_slug' => $adminPage['menu_slug'],
                'page_title' =>  $adminPage['page_title'],
                'menu_title' =>  ($title) ? $title : $adminPage['menu_title'],
                'capability' =>  $adminPage['capability'],
                'menu_slug' =>  $adminPage['menu_slug'],
                'callable' => $adminPage['callable']
            ]
        ];
        
        $this->AdminSubPages = $subPages;

        return $this;
    }

    public function AddSubAdminPages(array $pages)
    {
        $this->AdminSubPages = array_merge($this->AdminSubPages, $pages);

        return $this;
    }

    function AddAdminMenu()
    {
        foreach ($this->AdminPages as $page)
        {
            add_menu_page($page['page_title'], $page['menu_title'], $page['capability'],
                $page['menu_slug'], $page['callable'], $page['icon_url'], $page['position']);
        }

        foreach ($this->AdminSubPages as $page)
        {
            add_submenu_page($page['parent_slug'], $page['page_title'], $page['menu_title'], $page['capability'],
                $page['menu_slug'], $page['callable']);
        }
    }

    public function SetSettings(array $settings)
    {
        $this->Settings = $settings;

        return $this;
    }

    public function SetSections(array $sections)
    {
        $this->Sections = $sections;

        return $this;
    }

    public function SetFields(array $fields)
    {
        $this->Fields = $fields;

        return $this;
    }

    public function RegisterCustomFields()
    {
        // register settings
        foreach ($this->Settings as $setting)
        {
            register_setting($setting['option_group'], $setting['option_name'],
                (isset($setting['callback']) ? $setting['callback'] : ''));
        }

        // add settings section
        foreach ($this->Sections as $section)
        {
            add_settings_section($section['id'], $section['title'],
                (isset($section['callback']) ? $section['callback'] : ''),
                $section['page']);
        }

        // add settings field
        foreach ($this->Fields as $field)
        {
            add_settings_field($field['id'], $field['title'],
                (isset($field['callback']) ? $field['callback'] : ''),
                $field['page'], $field['section'],
                (isset($field['args']) ? $field['args'] : ''));
        }
    }
}