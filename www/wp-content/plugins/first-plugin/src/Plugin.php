<?php
/**
 * @package FirstPlugin
 */

namespace Src;

use Src\CustomPostTypes\CustomPost;

class Plugin
{
	static private Plugin $plugin;

	private final function  __construct() { }

	public static function GetPlugin()
	{
		if (!isset(self::$plugin))
		{
			self::$plugin = new Plugin();
		}

		return self::$plugin;
	}
	
	function Activation()
	{	
		//CustomPost::AddCustomPostType();
    
		flush_rewrite_rules();
	}

	function Deactivation()
	{
    	flush_rewrite_rules();
	}
}