<?php
/**
 * @package FirstPlugin
 */

namespace Src;

final class Init
{
	/**
	 * Store all the services classes inside an array
	 * @return array Array of services classes
	 */
	static public function GetServices()
	{
		return [
			Base\Enqueue::class,
			Base\SettingsLink::class,
			Pages\Admin::class,
			CustomPostTypes\CustomPost::class
		];
	}
	
	/**
	 * Register all the services
	 */
	static public function RegisterServices()
	{
		foreach	(self::GetServices() as $class)
		{
			$service = new $class();
			
            $service->Init();
			$service->Register();
		}
	}
}